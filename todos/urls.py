from django.urls import path
from todos.views import TodosListView, TodosItemCreate, TodosListCreate, TodosListDetailView

urlpatterns = [path ("", TodosListView.as_view(), name="todos_list"),
path("new/", TodosItemCreate.as_view(), name = "todositem_create"),
path("create/", TodosListCreate.as_view(), name = "todoslist_create"),
path("<int:pk>/", TodosListDetailView.as_view(), name = "todoslist_detail")]
from django.forms import ModelForm
from todos.models import TodoItem, TodoList

class ItemForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


    
from pprint import pprint
from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, DetailView
from todos.models import TodoList, TodoItem
from django.urls import reverse_lazy
from todos.forms import ItemForm

# Create your views here.
class TodosListView(ListView):
    model = TodoList
    template_name = "todos/listoflists.html"


class TodosListCreate(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]
    success_url = reverse_lazy("todos_list")



class TodosItemCreate(CreateView):
    model = TodoItem
    template_name = "items/create.html"
    fields = ["task", "due_date", "is_completed", "list"]
    print("gentle")
    
    #success_url = reverse_lazy("todos_list")
    def justprintit():
        print("banana")
    justprintit()
    
    def get_success_url(self):

        return reverse_lazy("todoslist_detail", args=[self.object.list.id])

    

class TodosItemUpdate(UpdateView):
    model = TodoList
    template_name = "items/edit.html"
    fields = ["name"]
    success_url = reverse_lazy("todos_list")

    # def fieldscheck(request, self):
    #     if request.method == 'POST':
    #         form = ItemForm(request.POST)
    #         if form.is_valid():
    #             return reverse_lazy("", args=[self.object.id])
    #         else:
    #             pass


class TodosListDetailView(DetailView):
    model = TodoList
    template_name = "todos/details.html"

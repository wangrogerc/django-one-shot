# Generated by Django 4.0.3 on 2022-05-03 20:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todos', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='TodoItem',
            new_name='TodosItem',
        ),
        migrations.RenameModel(
            old_name='TodoList',
            new_name='TodosList',
        ),
    ]
